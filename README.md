# A Rotary Tool

I've wanted to build a rotary tool for a minute, to knife-cut things, [wire plot like sam](https://gitlab.cba.mit.edu/calischs/wireplotting), and pick-and-place.

The idea is to have a 4th axis (rotating in the xy plane, the A axis) that we can add to machines real easy. One big through hole. Then, drop tools in the top of it. This is really very much like the Zund that Sam uses to plot wires.

![img](images/aaxis-in-purple-01.jpg)

![img](images/aaxis-in-purple-02.jpg)

This is two-part, here's an earlier sketch of the tool being removed from the axis:

![pullout](images/aaxis.jpg)

## BOM

```
#REF!
```

# Log

This is coming along, I've improved my tapered spline some (to have less contact, and a steeper taper) but I expect it will eventually anneal towards some kind of (more) kinematic joint. Kind of tricky piece of kit to do in a statisfying way, actually.

The other trick is learning how to print these teeth properly. Here's my attempt, after just drafting in fusion the profile exactly to spec:

![gt2](images/gt2.jpg)

![gt2close](images/gt2close.jpg)

![gt2close](images/gt2far.jpg)

The teeth should contact *first* at the trough of the belt (top of the pulley - this sets the diameter) and the 'tooth' of the belt should *just* nest into the pocket below it. Will do some CAD fanangling and try this again...

To update, I printed at 0.1mm layers (rather than 0.15mm previously) and I offset the 'tooth' geometry (but not the trough) by 0.075mm. I also have the Slic3r setting for 'external perimeters first' checked (as always!) so that things don't ooze outside of their meshes too much.

Looks like 0.05mm offset is best;

![gt2close](images/gt2-05off-close.jpg)

![gt2close](images/gt2-05off-far.jpg)

And here's Fusion's 'interference analysis' on the lower kinematic mount:

![contact](images/contact.png)
